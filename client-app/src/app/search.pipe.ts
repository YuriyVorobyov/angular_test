import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { Task4Component } from './task4/task4.component';
import { Observable } from 'rxjs';


type Message = string;

@Pipe({
 name: 'searchFilterSearch'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
 transform(words: Message[], keyword: string): Message[] {
    if (!words) { return []; }
    if (!keyword) { return words; }
    return words.filter(item => item.includes(keyword));
 }
}
