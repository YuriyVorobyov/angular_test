import { Component, OnInit, DoCheck } from '@angular/core';
import { SearchService } from '../services/search.service';

type Message = string;

@Component({
  selector: 'app-task4',
  templateUrl: './task4.component.html',
  styleUrls: ['./task4.component.css']
})
export class Task4Component implements OnInit, DoCheck {

  words: Message[];
  keyword = '';

  constructor(private searchService: SearchService) {
  }

  ngOnInit() {
    this.generateWords();
  }

  ngDoCheck() {
    this.generateWords();
    this.searchMessages(this.words, this.keyword);
  }

  generateWords(): void {
    this.words = ['word', 'owrd', 'orwd', 'ordw'];
  }

  searchMessages(words, keyword): void  {
    this.searchService.searchMessages(words, keyword)
      .subscribe(value => {
        this.words = value;
      });
  }

}
