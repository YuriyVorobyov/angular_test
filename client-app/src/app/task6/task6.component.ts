import { Component, OnInit } from '@angular/core';
import { ChatService } from '../services/chat.service';

@Component({
  selector: 'app-task6',
  templateUrl: './task6.component.html',
  styleUrls: ['./task6.component.css']
})
export class Task6Component implements OnInit {

  user: string;
  room: string;
  messageText: string;
  messageArray: Array <{ user: string, message: string }> = [];

  constructor(private chatService: ChatService) {
    this.chatService.newUserJoined()
      .subscribe(data => this.messageArray.push(data));

    this.chatService.userLeftRoom()
      .subscribe(data => this.messageArray.push(data));

    this.chatService.newMessageReceived()
      .subscribe(data => this.messageArray.push(data));
  }

  ngOnInit() {
  }

  join(): void {
    this.chatService.joinRoom({
      user: this.user,
      room: this.room
    });
  }

  leave(): void {
    this.chatService.leaveRoom({
      user: this.user,
      room: this.room
    });
  }

  sendMessage(): void {
    this.chatService.sendMessage({
      user: this.user,
      room: this.room,
      message: this.messageText
    });
  }

  clearInput(): void {
    this.messageText = '';
  }

}
