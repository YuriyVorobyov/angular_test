import { Component, OnInit } from '@angular/core';
import { User } from '../userClass/user';
import { AuthLogService } from '../services/auth-log.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  token: string;
  currentUser: User;
  user: object[];

  constructor(private authLogService: AuthLogService) {
  }

  ngOnInit() {
    this.token = localStorage.getItem('jwtToken');
  }

  logout(): void {
    this.token = null;
    localStorage.removeItem('jwtToken');
    localStorage.removeItem('user');
    this.authLogService.logout().subscribe((data: object[]) => {
        this.user = data;
    });
  }

  userInfo(): void {
    if (this.token != null) {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
    }
  }

}
