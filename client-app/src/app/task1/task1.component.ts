import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OnlineService } from '../services/online.service';

@Component({
  selector: 'app-task1',
  templateUrl: './task1.component.html',
  styleUrls: ['./task1.component.css']
})
export class Task1Component implements OnInit {

  onlineChecker: boolean;

  constructor(private http: HttpClient, private onlineService: OnlineService) {
  }

  ngOnInit() {
    this.getOnlineStatus();
  }

  getOnlineStatus(): void  {
    this.onlineService.getOnlineStatus()
      .subscribe(value => {
        this.onlineChecker = value;
        if (this.onlineChecker === true) {
          this.longPolling();
        }
      });
  }

  longPolling(): void {
    this.onlineService.longPolling()
      .subscribe();
  }

}
