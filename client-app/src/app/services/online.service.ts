import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval, of, fromEvent, merge } from 'rxjs';
import { map, switchMap, mapTo} from 'rxjs/operators';

@Injectable({
 providedIn: 'root'
})
export class OnlineService {

    online$: Observable<boolean>;

    constructor(private http: HttpClient) {

    }

    isUserLoggedIn$ = new Observable((observer) => {
        const currentUser = localStorage.getItem('jwtToken');
        if (currentUser != null) {
            observer.next(true);
        } else {
            observer.next(false);
        }
    });

    getOnlineStatus(): Observable<boolean> {
        return this.online$ = merge(
          of(navigator.onLine),
          fromEvent(window, 'online').pipe(mapTo(true)),
          fromEvent(window, 'offline').pipe(mapTo(false))
        );
    }

    longPolling(): Observable<void> {
        const inter = interval(10000);
        return inter.pipe( switchMap(() => this.http.get('http://192.168.1.99:3000/polling/connect')) )
           .pipe( map(data => console.log(data) ));
    }
}
