import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../userClass/user';

@Injectable({
 providedIn: 'root'
})
export class AuthRegService {
    public response: any;

    constructor(private http: HttpClient) { }

    public postData(user: User): any {
        const body = { login: user.login, password: user.password};
        console.log(body);
        return this.http.post('http://192.168.1.99:3000/auth/signup', body);
    }
}
