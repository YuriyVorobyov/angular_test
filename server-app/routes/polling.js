const express = require('express');
const router = new express.Router();

router.get('/connect', function (req, res) {
    setTimeout(()=>res.send(JSON.stringify('Long polling answer')), 2000);
});

module.exports = router;
