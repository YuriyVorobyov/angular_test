const express = require('express');
const router = new express.Router();
const usersController = require('../controllers/users');

router.get('/', usersController.list);
router.get('/:id', usersController.findUser);

module.exports = router;
